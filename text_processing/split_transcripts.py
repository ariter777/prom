#!/usr/bin/env python3
import sys, os
import pqdm.processes
import nltk.tokenize
import nltk
import re

cmudict = nltk.corpus.cmudict.dict()

def process_line(line):
    fn, _, transcript = line.strip().split("|")
    transcript = re.sub(r"[^A-Za-z\s]", "", transcript).lower()
    tokens = nltk.tokenize.word_tokenize(transcript)
    for i, token in enumerate(tokens):
        if token not in cmudict:
            for j in range(1, len(token)):
                if token[:j] in cmudict and token[j:] in cmudict:
                    tokens[i] = token[:j]
                    tokens.insert(i + 1, token[j:])
                    break
    with open(os.path.join(sys.argv[2], f"{fn}.txt"), "w") as wfd:
        wfd.write(' '.join(tokens))

if __name__ == "__main__":
    with open(sys.argv[1], "r") as rfd:
        pqdm.processes.pqdm(rfd, process_line, n_jobs=8, exception_behaviour="immediate")
