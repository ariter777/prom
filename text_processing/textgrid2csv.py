#!/usr/bin/env python3
import sys, os, glob
import pandas as pd
import textgrid
import pqdm.processes

def process_textgrid(filename):
    pd.DataFrame(textgrid.read_textgrid(filename)).to_csv(os.path.splitext(filename)[0] + ".csv")

if __name__ == "__main__":
    pqdm.processes.pqdm(glob.glob(os.path.join(sys.argv[1], "*.TextGrid")), process_textgrid, n_jobs=8)
