#!/usr/bin/env python3
import sys, os

import pandas as pd
import numpy as np
import tensorflow as tf
import json
import itertools
import re
import nltk
import syllabify
import train

with open("arpabet.json", "r") as rfd:
    arpabet_numbers = json.load(rfd)
lexicon = pd.read_csv("lexicon-norm-uniq.txt", sep=r'\t', engine="python", index_col=0, header=0).applymap(str.split)
lexicon.index = lexicon.index.map(str).map(str.lower)

def process_line(line):
    line = re.sub(r"[^A-Za-z,.:;!?\s]", "", line)
    tokens = [token.lower() if token not in ",.:;!?" else "<sil>" for token in nltk.tokenize.word_tokenize(line)]

    for i, token in enumerate(tokens):
        if token != "<sil>" and token not in lexicon:
            for j in range(1, len(token)):
                if token[:j] in lexicon and token[j:] in lexicon:
                    tokens[i] = token[:j]
                    tokens.insert(i + 1, token[j:])
                    break
    phonemes = [lexicon.loc[token]["transcription"] if token != "<sil>" else ["<sil>"] for token in tokens]
    phonemes_concat = list(itertools.chain.from_iterable(phonemes))

    df = pd.DataFrame({k: [] for k in train.X_FEATURES}).astype(train.X_FEATURES)
    df["phoneme"] = phonemes_concat
    df["pp_phoneme_code"] = list(map(lambda k: arpabet_numbers[k[:2] if not k.startswith("<s") else k], ["<sil>"] * 2 + phonemes_concat[:-2]))
    df["p_phoneme_code"] = list(map(lambda k: arpabet_numbers[k[:2] if not k.startswith("<s") else k], ["<sil>"] + phonemes_concat[:-1]))
    df["phoneme_code"] = list(map(lambda k: arpabet_numbers[k[:2] if not k.startswith("<s") else k], phonemes_concat))
    df["s_phoneme_code"] = list(map(lambda k: arpabet_numbers[k[:2] if not k.startswith("<s") else k], phonemes_concat[1:] + ["<sil>"]))
    df["ss_phoneme_code"] = list(map(lambda k: arpabet_numbers[k[:2] if not k.startswith("<s") else k], phonemes_concat[2:] + ["<sil>"] * 2))
    df["vowel"] = ((1 <= df["phoneme_code"]) & (df["phoneme_code"] <= 18)).astype(int)

    stress = 0 # stressed or not
    phoneme_idx = 0
    df["syl_idx"] = 0
    df["stress"] = 0
    for word_idx, word in enumerate(phonemes):
        if word != ["<sil>"]:
            for syl_idx, syllable in enumerate(syllabify.syllabify(word)):
                syllable = list(itertools.chain.from_iterable(syllable)) # no need for an onset-nucleus-coda split
                df.loc[phoneme_idx:phoneme_idx + len(syllable), "syl_idx"] = syl_idx
                df.loc[phoneme_idx:phoneme_idx + len(syllable), "stress"] = int(any(phoneme.endswith("1") for phoneme in syllable))
                df.loc[phoneme_idx:phoneme_idx + len(syllable), "word_idx"] = word_idx
                phoneme_idx += len(syllable)
        else:
            df.loc[phoneme_idx, "word_idx"] = word_idx
            phoneme_idx += 1
    return df.astype(train.X_FEATURES)

def eval_line(model, line):
    df = process_line(line)
    df["length"] = model.predict(df[train.X_FEATURES.keys()], use_multiprocessing=True)
    df["start"] = [0] + np.cumsum(df["length"])[:-1].tolist()
    df["end"] = np.cumsum(df["length"])
    return df

if __name__ == "__main__":
    model = tf.keras.models.load_model(sys.argv[1])
    result = eval_line(model, sys.argv[2])
    result.to_csv(sys.argv[3], index=False, sep='\t')
