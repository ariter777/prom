#!/usr/bin/env python3
import sys, os, glob
import numpy as np
import pandas as pd
import json
import pqdm.processes
import syllabify
import nltk
import itertools

with open("arpabet.json", "r") as rfd:
    arpabet_numbers = json.load(rfd)

lexicon = pd.read_csv("lexicon-norm-uniq.txt", sep=r'\t', engine="python", index_col=0, header=0).applymap(str.split)
lexicon.index = lexicon.index.map(str).map(str.lower)

def process_file(filename):
    try:
        df = pd.read_csv(filename).fillna("<sil>").drop("Unnamed: 0", axis=1)
        df["word"] = "<sil>"
        df["word_idx"] = 0
        df["stress"] = 0
        df["syl_idx"] = 0
        words = df[df["tier"] == "words"].iterrows()
        df = df[df["tier"] == "phones"].drop(["tier"], axis=1) # leave only phonemes
        df["length"] = df["stop"] - df["start"]
        df["pp_phoneme_code"] = list(map(lambda k: arpabet_numbers[k[:2] if not k.startswith("<s") else k], ["<sil>"] * 2 + df["name"][:-2].tolist()))
        df["p_phoneme_code"] = list(map(lambda k: arpabet_numbers[k[:2] if not k.startswith("<s") else k], ["<sil>"] + df["name"][:-1].tolist()))
        df["phoneme_code"] = list(map(lambda k: arpabet_numbers[k[:2] if not k.startswith("<s") else k], df["name"]))
        df["s_phoneme_code"] = list(map(lambda k: arpabet_numbers[k[:2] if not k.startswith("<s") else k], df["name"][1:].tolist() + ["<sil>"]))
        df["ss_phoneme_code"] = list(map(lambda k: arpabet_numbers[k[:2] if not k.startswith("<s") else k], df["name"][2:].tolist() + ["<sil>"] * 2))
        df["vowel"] = ((1 <= df["phoneme_code"]) & (df["phoneme_code"] <= 18)).astype(int)
        word = next(words)[1]
        syllables = [list(itertools.chain.from_iterable(syllable)) for syllable in (syllabify.syllabify(lexicon.loc[word["name"]]["transcription"]) if word["name"] != "<sil>" else [])]
        word_idx = 0 # what word we're in
        syl_idx = 0 # what syllable we're in
        syl_phone_idx = 0 # what phoneme we're in inside the syllable
        stress = 0 # stressed or not
        for i, phoneme in df.iterrows():
            if phoneme["stop"] > word["stop"]: # this phoneme belongs to the next word
                df.loc[(df["word_idx"] == word_idx) & (df["syl_idx"] == syl_idx), "stress"] = stress
                word = next(words)[1]
                syl_idx = 0 # we're back to the first syllable
                syl_phone_idx = 0 # first phoneme within said syllable
                syllables = [list(itertools.chain.from_iterable(syllable)) for syllable in (syllabify.syllabify(lexicon.loc[word["name"]]["transcription"]) if word["name"] != "<sil>" else [])]
                word_idx += 1
            if (syl_idx < len(syllables) and syl_phone_idx >= len(syllables[syl_idx])) or syl_idx >= len(syllables): # move on to the next syllable
                df.loc[(df["word_idx"] == word_idx) & (df["syl_idx"] == syl_idx), "stress"] = stress
                stress = 0
                syl_idx += 1 # next syllable
                syl_phone_idx = 0 # first syllable thereof
            if phoneme["name"].endswith("1"): # stressed syllable
                stress = 1
            df.loc[i, "word"] = word["name"]
            df.loc[i, "word_idx"] = word_idx
            df.loc[i, "syl_idx"] = syl_idx
            syl_phone_idx += 1

        df = df.rename(columns={"name": "phoneme"}).astype({"phoneme_code": "int16", "syl_idx": "int16"})
        df.to_csv(os.path.splitext(filename)[0] + ".feats", index=False, sep='\t')
        return None
    except Exception as e:
        return filename, repr(e)

if __name__ == "__main__":
    if os.path.isfile(sys.argv[1]):
        print(process_file(sys.argv[1]))
    elif os.path.isdir(sys.argv[1]):
        filenames = glob.glob(os.path.join(sys.argv[1], "*.csv"))
        blacklist = list(filter(None, pqdm.processes.pqdm(filenames, process_file, n_jobs=8, exception_behaviour="immediate")))
        with open(sys.argv[2], "w") as wfd:
            wfd.writelines('\n'.join(":\t".join(item) for item in blacklist))
        sys.stderr.write(f"Blacklisting {len(blacklist)} files; list in {sys.argv[2]}.\n")
