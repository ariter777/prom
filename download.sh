#!/usr/bin/env bash
echo $'\e[92;1mHello Talpiot\e[0m'
set -v
apt-get install pbzip2 tar

pip install nltk pandas numpy scipy pqdm
python -c 'import nltk; nltk.download("cmudict"); nltk.download("punkt")'

git clone https://github.com/kylebgorman/syllabify
mv syllabify/*.py .
rm -rf syllabify

wget 'http://drive.google.com/uc?export=download&id=1qqOaDrgcfnHLxAF-_xsVop1fcynHJbVU&confirm=t'
tar --use-compress-program=pbzip2 -xf LJSpeech-1.1.tar.bz2

echo "$'\e[92;1mDone.\e[0m'
