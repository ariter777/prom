#!/usr/bin/env python3
import sys, os, glob
import pqdm.threads

import numpy as np
import pandas as pd
import json
import tensorflow as tf
import sklearn.model_selection

X_FEATURES = {"word_idx": np.int32, "syl_idx": np.int32, "stress": np.int32, "pp_phoneme_code": np.int32, "p_phoneme_code": np.int32,
              "phoneme_code": np.int32, "s_phoneme_code": np.int32, "ss_phoneme_code": np.int32, "vowel": np.int32, "length": np.float32}
Y_FEATURES = {"mel": np.ndarray}

MEL_FRAME_LENGTH = 0.0231 # seconds

EMPTY_DF = pd.DataFrame({feat: [] for feat in X_FEATURES.keys() | Y_FEATURES.keys()})

def load_file(filename, blacklist):
    if not filename in blacklist:
        df = pd.read_csv(filename + ".feats", sep='\t', header=0, usecols=X_FEATURES.keys())
        with open(filename + ".npy", "rb") as rfd:
            df["mel"] = np.load(rfd)
    else:
        return EMPTY_DF

def load_data(data_dir, blacklist):
    filenames = [os.path.splitext(filename)[0] for glob.glob(os.path.join(data_dir, "*.feats")) if os.path.isfile(os.path.splitext(filename)[0] + ".npy")]
    if not filenames:
        raise ValueError("No files found.")
    sys.stderr.write("Loading data.\n")
    data = pd.concat(pqdm.threads.pqdm(filenames,
                                    lambda filename: load_file(filename, blacklist),
                                    n_jobs=8, exception_behaviour="immediate"))
    x, y = data[X_FEATURES.keys()], data[Y_FEATURES.keys()]
    x_train, x_test, y_train, y_test = sklearn.model_selection.train_test_split(x.astype(X_FEATURES), y.astype(Y_FEATURES), test_size=.15, random_state=1)
    x_test, x_valid, y_test, y_valid = sklearn.model_selection.train_test_split(x_test.astype(X_FEATURES), y_test.astype(Y_FEATURES), test_size=.25, random_state=1)
    return x_train, x_test, x_valid, y_train, y_test, y_valid

def setup_model():
    return tf.keras.models.Sequential([
        tf.keras.layers.InputLayer(input_shape=(len(X_FEATURES), 1)),
        tf.keras.layers.Dense(512, activation="relu"),
        tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(256, input_shape=(1, 1, 512))),
        tf.keras.layers.Dense(128, activation="relu"),
        tf.keras.layers.Dense(64, activation="relu"),
        tf.keras.layers.Dense(1)
    ])

if __name__ == "__main__":
    if len(sys.argv) != 4:
        sys.stderr.write(f"Usage: {sys.argv[0]} <data-dir> <blacklist-file> <model-dir>\n")
        sys.exit(1)
    with open(sys.argv[2], "r") as blacklist_rfd:
        blacklist = set(blacklist_rfd.read().splitlines())

    x_train, x_test, x_valid, y_train, y_test, y_valid = load_data(sys.argv[1], blacklist)

    cp_callback = tf.keras.callbacks.ModelCheckpoint(os.path.join(sys.argv[3], "checkpoints"), save_weights_only=True, verbose=1)
    es_callback = tf.keras.callbacks.EarlyStopping(restore_best_weights=True, patience=3, verbose=1)
    model = setup_model()
    model.summary()
    model.compile(optimizer="adam", loss="mse")

    model.fit(x_train, y_train, epochs=16, validation_data=(x_valid, y_valid), callbacks=[cp_callback, es_callback], use_multiprocessing=True, shuffle=False)
    model.evaluate(x_test, y_test, verbose=2)
    model_filename = os.path.join(sys.argv[3], "model.h5")
    model.save(model_filename)
    sys.stderr.write(f"Model saved to {model_filename}.\n")
