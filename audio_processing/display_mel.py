#!/usr/bin/env python3
import sys
import numpy as np
import librosa, librosa.display
import matplotlib.pyplot as plt

SR = 22050

def display_mel(filename):
    with open(filename, "rb") as rfd:
        mel = np.load(rfd)
    fig, ax = plt.subplots()
    mel_db = librosa.power_to_db(mel, ref=np.max)
    img = librosa.display.specshow(mel_db, x_axis="time", y_axis="mel", sr=SR, fmax=SR / 2, ax=ax, hop_length=SR // 100)
    fig.colorbar(img, ax=ax, format="%+2.0f dB")
    ax.set(title=filename)
    plt.show()

if __name__ == "__main__":
    display_mel(sys.argv[1])
