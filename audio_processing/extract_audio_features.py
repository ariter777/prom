#!/usr/bin/env python3
import sys, os, glob
import numpy as np
import librosa, librosa.display
import pqdm.processes

def process_file(filename):
    wav, sr = librosa.load(filename)
    mel = librosa.feature.melspectrogram(y=wav, sr=sr, fmax=sr / 2, hop_length=sr // 100)
    with open(os.path.splitext(filename)[0] + ".npy", "wb") as wfd:
        np.save(wfd, mel)

if __name__ == "__main__":
    if os.path.isfile(sys.argv[1]):
        process_file(sys.argv[1])
    elif os.path.isdir(sys.argv[1]):
        filenames = glob.glob(os.path.join(sys.argv[1], "*.wav"))
        pqdm.processes.pqdm(filenames, process_file, n_jobs=8, exception_behaviour="immediate")
